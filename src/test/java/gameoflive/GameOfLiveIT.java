package gameoflive;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class GameOfLiveIT {

	@Test
	public void smokeTest() throws InterruptedException {
		GameOfLive gameOfLive = new GameOfLive(140, 80);
		initByRandom(gameOfLive);
		int nextGeneration;
		do {
			dump(gameOfLive);
			TimeUnit.MILLISECONDS.sleep(100);
			nextGeneration = gameOfLive.nextGeneration();
		} while (nextGeneration > 0);
	}

	private void dump(GameOfLive gameOfLive) {
		System.out.println(gameOfLive);
		System.out.println();
	}

	private void initByRandom(GameOfLive gameOfLive) {
		Random random = new Random();
		List<Cell> cells = gameOfLive.getBoard().getCells();
		for (Cell cell : cells) {
			cell.setLife(random.nextBoolean());
		}
	}

}
