package gameoflive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GameOfLiveTest {

	@Test
	public void canCreateGame() {
		GameOfLive gameOfLive = new GameOfLive(5, 3);
		assertNotNull(gameOfLive);
	}

	@Test
	public void gameHasExpectedSize() {
		GameOfLive gameOfLive = new GameOfLive(2, 4);
		assertEquals(2, gameOfLive.getBoard().getWidth());
		assertEquals(4, gameOfLive.getBoard().getHeight());
	}

	@Test
	public void canCreateLife() {
		GameOfLive gameOfLive = new GameOfLive(5, 3);
		assertFalse(gameOfLive.isLifeAt(1, 2));
		gameOfLive.setLifeAt(1, 2);
		assertTrue(gameOfLive.isLifeAt(1, 2));
		assertFalse(gameOfLive.isLifeAt(2, 1));
	}

	@Test
	public void cellWithoutNeighbourWillDie() {
		GameOfLive gameOfLive = new GameOfLive(2, 2);
		gameOfLive.setLifeAt(0, 0);
		assertEquals(0, gameOfLive.nextGeneration());
		assertFalse(gameOfLive.isLifeAt(0, 0));
	}

	@Test
	public void cellWithTwoNeighboursWontDie() {
		GameOfLive gameOfLive = new GameOfLive(2, 2);
		gameOfLive.setLifeAt(0, 0);
		gameOfLive.setLifeAt(0, 1);
		gameOfLive.setLifeAt(1, 0);
		gameOfLive.nextGeneration();
		assertTrue(gameOfLive.isLifeAt(0, 0));
	}

	@Test
	public void cellWithThreeNeighboursWontDie() {
		GameOfLive gameOfLive = new GameOfLive(18, 22);
		gameOfLive.setLifeAt(4, 2);
		gameOfLive.setLifeAt(5, 2);
		gameOfLive.setLifeAt(4, 3);
		gameOfLive.setLifeAt(5, 3);
		gameOfLive.nextGeneration();
		assertTrue(gameOfLive.isLifeAt(4, 2));
		assertTrue(gameOfLive.isLifeAt(5, 2));
		assertTrue(gameOfLive.isLifeAt(4, 3));
		assertTrue(gameOfLive.isLifeAt(5, 3));
	}

	@Test
	public void cellWithFourNeighboursWillDie() {
		GameOfLive gameOfLive = new GameOfLive(10, 20);
		gameOfLive.setLifeAt(5, 5);

		gameOfLive.setLifeAt(4, 5);
		gameOfLive.setLifeAt(6, 5);

		gameOfLive.setLifeAt(5, 4);
		gameOfLive.setLifeAt(5, 6);

		gameOfLive.nextGeneration();
		assertFalse(gameOfLive.isLifeAt(5, 5));
	}

	@Test
	public void cellWithExactlyThreeNeighboursWillBeBorn() {
		GameOfLive gameOfLive = new GameOfLive(3, 3);
		gameOfLive.setLifeAt(0, 2);
		gameOfLive.setLifeAt(1, 2);
		gameOfLive.setLifeAt(2, 2);

		assertEquals(1, gameOfLive.nextGeneration());
		assertTrue(gameOfLive.isLifeAt(1, 1));
	}

	@Test
	public void testDumpBoard() {
		GameOfLive gameOfLive = new GameOfLive(3, 3);
		assertEquals("   \n" + "   \n" + "   \n", gameOfLive.toString());
		for (Cell cell : gameOfLive.getBoard().getCells()) {
			cell.setLife(true);
		}
		gameOfLive.getBoard().getCellAt(1, 1).setLife(false);
		assertEquals("***\n" + "* *\n" + "***\n", gameOfLive.toString());
	}

}
