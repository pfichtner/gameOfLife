package gameoflive;

import java.util.List;

public class GameOfLive {

	private Board board;

	public GameOfLive(int width, int height) {
		board = new Board(width, height);
	}

	public Board getBoard() {
		return board;
	}

	public void setLifeAt(int x, int y) {
		board.setLifeAt(x, y);
	}

	public boolean isLifeAt(int x, int y) {
		return board.isLifeAt(x, y);
	}

	public int nextGeneration() {
		Board next = new Board(board.getWidth(), board.getHeight());
		letDie(next);
		int newLifes = createNewLife(next);
		board = next;
		return newLifes;
	}

	private Board letDie(Board next) {
		for (Cell cell : getBoard().getCells()) {
			if (cell.isLife()) {
				int neighbours = countLivingNeighbours(cell);
				if (neighbours == 2 || neighbours == 3) {
					next.setLifeAt(cell.getX(), cell.getY());
				}
			}
		}
		return next;
	}

	private int createNewLife(Board next) {
		int newLifes = 0;
		for (Cell cell : getBoard().getCells()) {
			if (!cell.isLife()) {
				int neighbours = countLivingNeighbours(cell);
				if (neighbours == 3) {
					next.setLifeAt(cell.getX(), cell.getY());
					newLifes++;
				}
			}
		}
		return newLifes;
	}

	private int countLivingNeighbours(Cell cell) {
		List<Cell> neighbours = cell.getNeighbours();
		int lifeCnt = 0;
		for (Cell neighbour : neighbours) {
			if (neighbour.isLife()) {
				lifeCnt++;
			}
		}
		return lifeCnt;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int x = 0; x < board.getWidth(); x++) {
			for (int y = 0; y < board.getHeight(); y++) {
				Cell cellAt = board.getCellAt(x, y);
				sb.append(cellAt.isLife() ? "*" : " ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

}
