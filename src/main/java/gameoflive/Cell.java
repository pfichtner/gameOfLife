package gameoflive;

import java.util.List;

public class Cell {

	private boolean life;
	private List<Cell> neighbours;
	private int x;
	private int y;

	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setLife(boolean life) {
		this.life = life;

	}

	public boolean isLife() {
		return life;
	}

	public List<Cell> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(List<Cell> neighbours) {
		this.neighbours = neighbours;
	}

}
