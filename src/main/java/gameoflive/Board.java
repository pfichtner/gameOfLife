package gameoflive;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {

	private final int width;
	private final Cell[] cells;
	private List<Cell> cells_;

	public Board(int width, int height) {
		this.width = width;
		cells = new Cell[width * height];
		initCells(width, height);
	}

	private void initCells(int width, int height) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				cells[y * width + x] = new Cell(x, y);
			}
		}
		for (Cell cell : cells) {
			cell.setNeighbours(detectNeighboursOf(cell));
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return cells.length / width;
	}

	public void setLifeAt(int x, int y) {
		getCellAt(x, y).setLife(true);
	}

	public boolean isLifeAt(int x, int y) {
		return getCellAt(x, y).isLife();
	}

	public Cell getCellAt(int x, int y) {
		return cells[indexOf(x, y)];
	}

	private List<Cell> detectNeighboursOf(Cell cell) {
		List<Cell> neighboursOf = new ArrayList<Cell>(8);
		int cx = cell.getX();
		int cy = cell.getY();
		for (int x = max(0, cx - 1); x <= min(cx + 1, getWidth() - 1); x++) {
			for (int y = max(0, cy - 1); y <= min(cy + 1, getHeight() - 1); y++) {
				if (x != cx || y != cy) {
					neighboursOf.add(getCellAt(x, y));
				}
			}
		}
		return neighboursOf;
	}

	private int indexOf(int x, int y) {
		return y * width + x;
	}

	public List<Cell> getCells() {
		if (cells_ == null) {
			// push init :-/
			getCellAt(0, 0);
			cells_ = Arrays.asList(cells);
		}
		return cells_;

	}

}
